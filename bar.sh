DATE=$(date "+%m.%d %H:%M:%S")
DISK=$(df -h | grep "/$" | sed 's/%//g' | awk '{print ($5 > 50) ? " ": ""}')
MEM=$(free --si | grep Mem | sed 's/[MG]//g' | awk '{print ($3 < 4) ? " " : ""}')
CPU=$(mpstat -P ALL | awk '(NR>=5 && $NF<=50) {count++} END {print (count) ? " " : ""}')
BAT="$(acpi -b | grep -v unavailable 2> /dev/null)"
BAT=$([[ -n $BAT ]] && echo $BAT | sed 's/,//g' | awk '{ print ($3 == "Discharging") ? " " $4 " (~"$5")" : ($3 == "Charging") ? " " $4 " ("$5")" : "" }')
CONT=$(cat /tmp/incus_container)

echo  "" $CONT $BAT $CPU $MEM $DISK $DATE
