# i3wm-config

Configuration files for i3 and Sway.

- i3: clone this repo inside de `.config` folder
- sway: same as i3, but link the `sway` folder to `~/.config/sway`
